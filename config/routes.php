<?php
	return array(     
			"user/login" => "user/login",
			"user/logout" => "user/logout",
			
			"task/create" => "task/create",
			"task/edit/(\d+)" => "task/edit/$1",
			"task/delete/(\d+)" => "task/delete/$1",
			
			"page-(\d)" => "task/index/$1",
			"" => "task/index",
	);