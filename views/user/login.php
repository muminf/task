<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Система управления кафе</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- Font Awesome -->
      <link rel="stylesheet" href="../resources/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="../resources/css/ionicons.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="../resources/dist/css/adminlte.min.css">
      <!-- iCheck -->
      <link rel="stylesheet" href="../resources/plugins/iCheck/square/blue.css" >
      <!-- Google Font: Source Sans Pro -->
      <link rel="stylesheet" href="../resources/css/fonts.css" >
    </head>
<body>
<div class="login-box">
  <div class="login-logo">
    <b>Task</b>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Авторизуйтесь чтобы войти в систему</p>
    <?php if(isset($errors) && is_array($errors)):?>
       
    <div class="alert alert-danger alert-dismissible" style="font-size:0.8em;">
        <h5><i class="icon fa fa-ban"></i> Ошибка авторизации!</h5>
        <?php foreach($errors as $error):?>
            <li><?php echo $error;?></li>
        <?php endforeach;?>
    </div>
    <?php endif;?>
      <form action="#" method="post">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fa fa-user"></i></span>
            </div>
            <input type="text" class="form-control" autocomplete="off" name="login" placeholder="Логин" value="<?php echo $login?>">
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fa fa-lock"></i></span>
            </div>
            <input type="password" class="form-control" name="password" placeholder="Пароль" value="<?php echo $password?>">
        </div>
        <div class="row">
          
          <!-- /.col -->
          <div class="col-4" style="margin:auto">
            <button type="submit" class="btn btn-primary btn-block btn-block">Войти</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

    
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<style>
    body{
        background: #e9ecef;
    }
</style>
<!-- /.login-box -->
<!-- jQuery -->
<script src="../../resources/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../resources/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../../resources/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../resources/dist/js/demo.js"></script>
</body>
</html>



