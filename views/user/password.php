<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Система управления кафе</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- Font Awesome -->
      <link rel="stylesheet" href="../template/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="../template/css/ionicons.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="../template/dist/css/adminlte.min.css">
      <!-- iCheck -->
      <link rel="stylesheet" href="../template/plugins/iCheck/square/blue.css" >
      <!-- Google Font: Source Sans Pro -->
      <link rel="stylesheet" href="../template/css/fonts.css" >
    </head>
<body>
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>shop</b>1.0</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Смена пароля</p>
      <form action="#" method="post">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fa fa-user"></i></span>
            </div>
            <input type="text" class="form-control" autocomplete="off" readonly="1" name="login" placeholder="Логин" value="<?php echo $login?>">
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fa fa-lock"></i></span>
            </div>
            <input type="password" minlength="3" maxlength="10" required="true" class="form-control" name="password" placeholder="Пароль">
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fa fa-lock"></i></span>
            </div>
            <input type="password" minlength="3" maxlength="10" required="true" class="form-control" name="password2" placeholder="Повторите пароль">
        </div>
        <div class="row">
          
          <!-- /.col -->
          <div class="col-4" style="margin:auto">
            <button type="submit" class="btn btn-primary btn-block btn-block">Сохранить</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

    
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<style>
    body{
        background: #e9ecef;
    }
</style>
<!-- /.login-box -->
<!-- jQuery -->
<script src="../../template/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../template/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../../template/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../template/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../template/dist/js/demo.js"></script>
</body>
</html>



