<?php include ROOT . '/views/layouts/header.php'; ?>
<div class="row pt-5">
    <div class="col-md-6" style="margin-left:auto;margin-right:auto;">
        <div class="card card-primary  ">
            <div class="card-header sm">
            <h3 class="card-title">Создание задачи</h3>
            </div>
            <form role="form" action="" method="post">
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Пользователь</label>
                        <input type="text" class="form-control" name="username" required minlength="2">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="text" class="form-control" name="email" required minlength="6">
                    </div>

                    <div class="form-group">
                        <label for="description">Описание задачи</label>
                        <textarea name="description" class="form-control" rows="5" required placeholder="Введите текст задачи"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" name="create" class="btn btn-primary float-right">
                        <input type="button" class="btn btn-default float-right" value="Отмена" onclick="history.back();">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include ROOT . '/views/layouts/footer.php'; ?>