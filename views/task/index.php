<?php include ROOT . '/views/layouts/header.php'; ?>
<div class="row pt-3">
    <div class="col-md-9" style="margin-left:auto;margin-right:auto;">
        <div class="card card-primary  ">
            <div class="card-header sm">
            <h3 class="card-title">Сортировка</h3>
            </div>
            <form role="form" action="/" method="post">
                <div class="card-body">
               
                    <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <button type="button" class="btn btn-danger">Поле</button>
                    </div>
                    <!-- /btn-group -->
                    <select name="key" class="form-control col-2">
                            <option value="username">Пользователь
                            <option value="email" <?=(isset($_SESSION["filter"]["key"]) && $_SESSION["filter"]["key"] == "email" ? "selected" : "")?>>Email
                            <option value="executed" <?=(isset($_SESSION["filter"]["key"]) && $_SESSION["filter"]["key"] == "executed" ? "selected" : "")?>>Статус
                        </select>
                    
                    <div class="input-group-prepend">
                        <button type="button" class="btn btn-danger">Сортировка</button>
                    </div>
                    <!-- /btn-group -->
                    <select name="val" class="form-control col-2">
                            <option value="">Нет
                            <option value="DESC" <?=(isset($_SESSION["filter"]["val"]) && $_SESSION["filter"]["val"] == "DESC" ? "selected" : "")?>>По убыванию
                            <option value="ASC" <?=(isset($_SESSION["filter"]["val"]) && $_SESSION["filter"]["val"] == "ASC" ? "selected" : "")?>>По возрастанию
                        </select>
                    
                        <input type="submit" name="sort" class="btn btn-primary" value="Применить">
                        <input type="submit" name="reset_filter" class="btn btn-default" value="Очистить">
                    </div> 
                </div>
            </form>
        </div>
    </div>
</div>

<section>
    <div class="container">
    <div class="row">
        <div class="col-md-4">
            <a href="task/create"><button class="btn btn-success">Добавить задачу</button></a>
        </div>
        <div class="col-md-8">
            <?php echo $pagination->get(); ?>
        </div>
    </div>
    <div class="row pt-3">

                <div class="col-sm-12 col-sm-offset-12 padding-right">
                    <?php foreach($tasks as $task): ?>
                        <div class="card">
                            <div class="card-header" style="background-color:<?=($task["executed"]>0 ? '#6efb8e' : '#ffffff')?>">
                                <strong><?=$task["username"]; ?></strong> [<a href="mailto:<?=$task["email"];?>"><?=$task["email"];?></a>]
                            </div>
                            <div class="card-body">
                                <blockquote class="blockquote mb-0">
                                <p><?=$task["description"]; ?></p>
                                <?php if(!User::isGuest()): ?>
                                    <footer class="blockquote-footer">
                                        <a href="/task/edit/<?=$task["id"]; ?>">Редактировать</a>
                                        <a class="delete" id="<?=$task["id"]; ?>" href="#">Удалить</a>
                                    </footer>
                                <?php endif; ?>
                                </blockquote>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $pagination->get(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>