<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="/resources/dist/img/AdminLTELogo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Task</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
     
    <?php if(!User::isGuest()):?>    
   
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="/resources/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
              <p class="d-block" style="color:#ffffff;margin-top:-10px;"><?php echo $_SESSION["user"]["name"]; ?></a>
            <a href="/user/logout" class="d-block" style="margin-top:8px">
                <i class="nav-icon fa fa-lock"></i>  Выйти
            </a>
          </div>
        </div>

      <?php else: ?>
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="/resources/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="/user/login" class="d-block" style="margin-top:8px">
                <i class="nav-icon fa fa-lock"></i>  Авторизация
            </a>
          </div>
        </div>

      <?php endif; ?>
      </nav>
      <!-- /.sidebar-menu -->
      
    </div>
    <!-- /.sidebar -->
  </aside>


