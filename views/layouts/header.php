<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Система управления кафе</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- Font Awesome -->
      <link rel="stylesheet" href="/resources/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="/resources/css/ionicons.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="/resources/dist/css/adminlte.min.css">
      <!-- iCheck -->
      <link rel="stylesheet" href="/resources/plugins/iCheck/square/blue.css" >
      <!-- Google Font: Source Sans Pro -->
      <link rel="stylesheet" href="/resources/css/fonts1.css" >
      
      
      <!-- Select2 -->
      <link rel="stylesheet" href="/resources/plugins/select2/select2.min.css">
      
      <link rel="stylesheet" href="/resources/css/style.css">
      
      <link rel="stylesheet" href="/resources/plugins/datepicker/datepicker3.css">
      
      <link rel="stylesheet" href="/resources/plugins/datatables/jquery.dataTables.min.css">
      
      <link rel="stylesheet" href="/resources/css/bootstrap-toggle.min.css">
      
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<div class="wrapper">
  
<?php #include_once 'navbar.php';?>
<?php include_once 'left_menu.php';?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   
    <!-- /.content -->
  <?php if(isset($_SESSION["msg"])): ?>
    <div class="row">
        <div class="col-12">
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <?=$_SESSION["msg"];?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
        </div>
    </div>
  <?php 
    unset($_SESSION["msg"]);
    endif ?>
 