<?php
session_start();
date_default_timezone_set('Asia/Dushanbe');

define("ROOT", dirname(__FILE__));
require_once(ROOT."\components\Autoload.php");
$router = new Router();
$router->run();
?>