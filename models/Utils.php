<?php

class Utils
{
    public static function pre($data)
    {
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }

    public static function isAssoc($array){
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }
    
    function insert($tbl,$data){
        $db = Db::getConnection();
        $fld = $val = "";
        if(self::isAssoc($data)){ // Ассоциативный одномерный массив для однострочной вставки
            foreach($data as $k=>$v){
                $fld .= $k.",";
                if(is_array($v)){
                    $v = implode(",",$v);
                    $val .= "',".$v.",',";
                }else{
                    $val .= "'".$v."',";
                }
            }
            $fld = substr($fld,0,-1);
            $val = substr($val,0,-1);
            $sql = "INSERT INTO ".$tbl."($fld) VALUES($val)";
        }else{				// Многомерный массив для многострочной вставки
            foreach($data as $k=>$v){
                $fld = "";
                $val .= "(";
                foreach($v as $kk=>$vv){
                    $fld .= $kk.",";
                    if(is_array($vv)){
                        $vv = implode(",",$vv);
                    }
                    $val .= "'".$vv."',";
                }
                $val = substr($val,0,-1);
                $val .= "),";
                
            }
            $fld = substr($fld,0,-1);
            $val = substr($val,0,-1);
            $sql = "INSERT INTO ".$tbl."($fld) VALUES$val";
        } 
        $db->Query($sql);
        return $db->lastInsertId();
    }

    function update($tbl,$data,$key = "id"){
        $db = Db::getConnection();
        if(self::isAssoc($data)){ // Ассоциативный одномерный массив для однострочной вставки
            $sql = "UPDATE ".$tbl." SET ";
            $cond = "";
            foreach($data as $k=>$v){
                if($k == $key){
                    $cond = " WHERE ".$k." = ".$v;
                }else{
                    $sql .= $k." = '".$v."',";
                }
            }
            $sql = substr($sql,0,-1);
            $sql .= $cond;
            $db->Query($sql); 
            return 1;
        }else{							// Многомерный массив для многострочного обновления
            foreach($data as $k=>$v){
                $sql = "UPDATE ".$tbl." SET ";
                $cond = "";
                foreach($v as $kk=>$vv){
                    if($kk == $key){
                        $cond = " WHERE ".$kk." = ".$vv;
                    }else{
                        $sql .= $kk." = '".$vv."',";
                    }				
                }
                $sql = substr($sql,0,-1);
                $sql .= $cond;
                $db->Query($sql);
            }
            return 1;
        }
    }

    function delete($tbl,$data,$key="id"){
        $db = Db::getConnection();
        if(is_array($data) && count($data)>0){
            $ids = implode(",",$data);
            $sql = "DELETE FROM ".$tbl." WHERE ".$key." in('".$ids."')";
        }else{
            $sql = "DELETE FROM ".$tbl." WHERE ".$key." = '".intval($data)."'";
        }
        return $db->Query($sql);
    }
    
    public static function getLastId($tbl_name)
    {
        $db = Db::getConnection();
        $sql = "SELECT id FROM ".$tbl_name." ORDER by id DESC LIMIT 1";
        $res = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
        return $res["id"];
    }
    
    public static function dateFormat($date, $format='Y-m-d')
    {
        return date($format,strtotime($date));
    }
    
}
?>