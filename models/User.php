<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author M_Fayziev
 */
class User {
    
    const tbl_name = 'users';
    const salt = 'Larсаывaw3*1vsv/*-vdac+)(*&^5';

    public static function register($name,$login,$password)
    {
       $db = Db::getConnection();
       $sql = 'INSERT INTO '.self::tbl_name.'(name,login,password) '
                .' VALUES(:name,:login,:password)';
       $result = $db->prepare($sql);
       $password = self::getPasswordHash($password);
       $result->bindParam(':name', $name, PDO::PARAM_STR);
       $result->bindParam(':login', $login, PDO::PARAM_STR);
       $result->bindParam(':password', $password, PDO::PARAM_STR);
       return $result->execute();         
    }
    
    public static function checkLogin($login,$password)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM '.User::tbl_name.' WHERE login = :login AND password = :password';
        $password = self::getPasswordHash($password);
        $result = $db->prepare($sql);
        $result->bindParam(':login',$login,PDO::PARAM_STR);
        $result->bindParam(':password',$password,PDO::PARAM_STR);
        $result->execute();
        $user = $result->fetch();
        if($user){
            return $user;
        }
        return false;
    }
    
    public static function isGuest()
    {
        if(!isset($_SESSION['user'])){
            return true;
        }
        return false;
    }

    
    public static function edit($userId,$name,$password)
    {
        $db = Db::getConnection();
        $sql = 'UPDATE user SET name = :name, password = :password '
                .' WHERE id = :id';
       $result = $db->prepare($sql);
       $result->bindParam(':id', $userId, PDO::PARAM_INT);
       $result->bindParam(':name', $name, PDO::PARAM_STR);
       $result->bindParam(':password', $password, PDO::PARAM_STR);
       return $result->execute();         
    }
        
    public static function getPasswordHash($password){
        return md5($password.self::salt);
    }

    public function checkAccess()
    {
        if(User::isGuest()){
			header("Location:/");
		}
    }
}
