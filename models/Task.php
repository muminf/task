<?php

class Task
{
    const tbl_name = 'tasks';
    const task_show_count = 3;

    public static function getList($page,$filter=array())
    {
       $db = Db::getConnection();
       $page = (intval($page)>1 ? intval($page-1)*3 : intval($page) - 1);
       $sql = 'SELECT * FROM '.self::tbl_name;

       if(count($filter)>0 && strlen($filter["val"])>0){
           $sql .= " ORDER BY ".$filter["key"]." ".$filter["val"];
       }else{
            $sql .= " ORDER BY id DESC";
       }

       $sql .= " LIMIT ".$page.",".self::task_show_count;

       $query = $db->query($sql);
       $list = array();
       while($res = $query->Fetch(PDO::FETCH_ASSOC)){
            $list[] = $res;
       }
        return $list;
    }

    public function getTaskById($task_id)
	{
        $db = Db::getConnection();
        $sql = "SELECT * FROM ".self::tbl_name." WHERE id = ".intval($task_id)." LIMIT 1";
        return $db->query($sql)->fetch(PDO::FETCH_ASSOC);
	}

    public function getTotalCount()
	{
        $db = Db::getConnection();
        $sql = "SELECT count(*) total FROM ".self::tbl_name;
        $res = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
        return $res["total"];
	}
}
?>