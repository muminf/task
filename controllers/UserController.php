<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author M_Fayziev
 */
class UserController {
    
    const tbl_name = 'users';

    public function actionLogin()
    {
        if(!User::isGuest()){
            header("Location:/");
        }

        $login = '';
        $password = '';
        if(isset($_POST['login'])){
            $login = substr($_POST['login'],0,10);
            $password = $_POST['password'];
            $errors = false;
           
            $user = User::checkLogin($login,$password);
            if($user == false){
                $errors[] = 'Неправильные данные для входа на сайт';
            }else{
                $_SESSION["user"] = $user;
                header("Location:/");
            }
        }
        require_once ROOT.'/views/user/login.php';
        return true;
    }
    public function actionLogout()
    {
        unset($_SESSION['user']);
        header('Location:/shop/');
    }
}
    
