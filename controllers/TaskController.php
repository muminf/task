<?php
include_once ROOT.'/components/Pagination.php';
class TaskController
{
	public function actionIndex($page = 1)
	{
		if(isset($_POST["sort"]) && strlen($_POST["val"])>0){
			$_SESSION["filter"] = array(
				"key" => $_POST["key"],
				"val" => $_POST["val"]
			);
		}

		if(isset($_POST["reset_filter"])){
			$_SESSION["filter"] = array();
		}

		$tasks = Task::getList($page,$_SESSION["filter"]);
		$total = Task::getTotalCount();
		$pagination = new Pagination($total, $page, Task::task_show_count, "page-");
		require_once ROOT.'/views/task/index.php';
        return true;
		
	}
	
	public function actionCreate()
	{
		if(isset($_POST["create"])){
			$new = array(
				"username" => $_POST["username"],
				"email" => $_POST["email"],
				"description" => $_POST["description"],
				"created_at" => date("Y-m-d H:i:s"),
			);

			//Если зарегистрированный пользователь, то добавляем ID
			if(!User::isGuest()){
				$new["created_user"] = $_SESSION["user"]["id"];
			}

			//Сохраняем задачу в БД
			Utils::insert(Task::tbl_name, $new);

			//Отправляем пользователю уведомление о добавлении заявки
			$_SESSION["msg"] = "Заявка успешно добавлена.";
			header("Location:/");
		}
		
		require_once ROOT.'/views/task/create.php';
        return true;
	}

	
	public function actionEdit($task_id)
	{ 
		//Проверяем доступ
		User::checkAccess();

		if(isset($_POST["edit"])){

			$edit = array(
				"id" => $task_id,
				"description" => $_POST["description"]
			);

			$edit["executed"] = 0;
			if(isset($_POST["executed"])){
				$edit["executed"] = 1;
				$edit["exec_time"] = date("Y-m-d H:i:s");
				$edit["exec_by"] = $_SESSION["user"]["id"];
			}

			//Обновляем задачу
			Utils::update(Task::tbl_name,$edit);

			//Отправляем пользователю уведомление об обновлении
			$_SESSION["msg"] = "Задача №".$task_id." обновлена.";

			header("Location:/");
		}

		$task = Task::getTaskById($task_id);
		require_once ROOT.'/views/task/edit.php';
        return true;
	}

	
	public function actionDelete($task_id)
	{
		//Проверяем доступ
		User::checkAccess();

		//Удаляем задачу с таким ID
		Utils::delete(Task::tbl_name, intval($task_id));

		//Отправляем пользователю сообщение об удалении
		$_SESSION["msg"] = "Задача №".$task_id." удалена.";
		header("Location:/");
	}

	
}
?>