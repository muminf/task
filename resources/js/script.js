$(document).ready(function() {
    $(".delete").click(function(e){
        e.preventDefault();
        var id = $(this).attr("id");
        if(confirm("Вы действительно хотите удалить эту задачу?")){
            location.href =  "/task/delete/"+id;
        }
    });
});