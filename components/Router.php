<?php

class Router
{
	private $routes;
	
	//Инициализируем роуты
	public function __construct()
	{
		$routesPath = ROOT."/config/routes.php";
		$this->routes = include($routesPath);
	}
	
	// Получаем строку запроса
	private function getURI()
	{
		if(!empty($_SERVER["REQUEST_URI"])){
			return trim($_SERVER["REQUEST_URI"],"/");
		}
	}
	
	//Проверяем существование контроллера и метода
	private function checkRoute($controllerObject,$actionName)
	{
		if(is_object($controllerObject)){
			return method_exists($controllerObject,$actionName);
		}
		return false;
	}
	
	//Запускаем роутер
	public function run()
	{
		$uri = $this->getURI();
		
		foreach($this->routes as $uriPattern => $path){
			if(preg_match("~$uriPattern~",$uri)){
				
				$internalRoute = preg_replace("~$uriPattern~",$path,$uri);
				
				$segments = explode("/", $internalRoute);
			
				$controllerName = ucfirst(array_shift($segments))."Controller";
				$actionName = "action".ucfirst(array_shift($segments));
				$parameters = $segments;
				$controllerFile = ROOT."/controllers/".$controllerName.".php";
				
				if(file_exists($controllerFile)){
					include_once($controllerFile);
				}
				$controllerObject = new $controllerName;
				$result = $this->checkRoute($controllerObject,$actionName);
				if($result != null){
					break;
				}
			}
		}

		//Если контроллер и метод существует вызываем
		//в противном случае перенаправляем на главную страницу
		if($result){
			call_user_func_array(array($controllerObject,$actionName),$parameters);
		}else{
			header("Location:/");
		}
	}
}

?>